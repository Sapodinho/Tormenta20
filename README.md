# Tormenta20

This is an UNOFICIAL system made by fan and has no affiliation with Tormenta20 or Jambo Editora.
Tormenta 20 is a brazilian RPG system owned by Jambo Editora.

## Description

Build campaigns in the Tormenta20 using Foundry VTT!

## TODOS & UPDATES

* ~~Corrigir caixa de magias preparadas~~ (v0.9.2)
* ~~Adicionar novos ofícios e novas perícias~~ (v0.9.3)
* ~~Adicionar aba de biografia e imagem~~ (v0.9.3)
* ~~Equipar e desequipar itens~~ (v0.9.4)
* ~~Correção de bugs~~ (v0.9.4)
* ~~Arrumar macros~~ (v0.9.5)
* ~~Ficha de NPCS~~ (v0.9.6)
* ~~Separar equipamentos em armas, consumiveis e tesouro~~ (v0.9.7)
* ~~Melhorar ataques~~ (v0.9.7)
* ~~Aplicar dano/cura~~ (v0.9.7)
* ~~Correção de medição de distância diagonal~~ (v0.9.8)
* ~~Correções para atualização 0.7.5 do Foundry~~ (v0.9.82)
* ~~Correções para atualização 0.7.8 do Foundry~~ (v0.9.84)
* ~~Correções adicionais de ataques, medição de distância por etapas e melhor visualização de críticos~~ (v0.9.85)
* ~~Suporte a aprimoramentos das magias~~ (v0.9.9)
* ~~Personalizar rolagens usando atalhos do teclado (SHIFT, CTRL, ALT)~~ (v0.9.9)
* ~~Melhorias diversas~~ (v0.9.92)
* ~~Reformulação das armas e Macros de para Armas~~ (v0.9.93)
* ~~Itens Roláveis~~ (v0.9.93)
* Melhorar Poderes e Itens num geral
* Condições e efeitos ativos.
* Layout da ficha

## Colaboradores
* TheTruePortal
* Mateus Clemente
* Roberto Caetano
